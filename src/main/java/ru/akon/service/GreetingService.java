package ru.akon.service;

import io.quarkus.vertx.ConsumeEvent;
import jakarta.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;

@ApplicationScoped
public class GreetingService {

    private static final Logger LOG = Logger.getLogger(GreetingService.class);

    @ConsumeEvent(value = "greeting", blocking = true)
    public String greeting(String name) {
        LOG.info("consume bus: " + name);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return "Hello " + name;
    }

}