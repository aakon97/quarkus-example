package ru.akon.service;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;
import ru.akon.BeerResource;
import ru.akon.dto.Beer;

import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class BeerServiceImpl implements BeerService {

    private static final Logger LOG = Logger.getLogger(BeerServiceImpl.class);

    @Override
    public Uni<List<Beer>> getBeers(int page) {
        int size = 1;
        if (page > 10) {
            size = 0;
        }

        LOG.info("get data");
        return Uni.createFrom().item(generateBeerList(size));
    }

    @Override
    public Uni<List<Beer>> getBeer(int id) {
        return Uni.createFrom().item(generateBeerList(1));
    }

    private List<Beer> generateBeerList(int size) {
        List<Beer> list = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            Beer beer = Beer.of(
                    "Name",
                    "Tag",
                    0.7
            );
            list.add(beer);
        }
        return list;
    }


}
