package ru.akon.service;

import io.smallrye.mutiny.Uni;
import io.vertx.core.json.JsonArray;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import ru.akon.dto.Beer;

import java.util.List;

//@Path("/v2")
//@RegisterRestClient
public interface BeerService {

//    @GET
//    @Path("/beers")
//    @Produces(MediaType.APPLICATION_JSON)
    Uni<List<Beer>> getBeers( int page);

    Uni<List<Beer>> getBeer(@PathParam("id") int id);
}
