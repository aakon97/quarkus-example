package ru.akon;

import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.core.eventbus.EventBus;
import io.vertx.mutiny.core.eventbus.Message;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.jboss.logging.Logger;

@Path("/hello")
public class GreetingResource {

    private static final Logger LOG = Logger.getLogger(GreetingResource.class);

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello from Quarkus REST 1111";
    }


    @Inject
    EventBus bus;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/one")
    public String hello1(@QueryParam("execute") boolean execute) {
        LOG.info("start hello");
        Uni<String> uni1 = get1();
        Uni<String> uni2 = get2();

        if (execute) {
            print(uni1);
        } else {
            print(uni2);
        }
        return "OK";
    }

    private Uni<String> get1() {
        return bus.<String>request("greeting", "First call")
                .onItem().transform(Message::body);
    }

    private Uni<String> get2() {
        return bus.<String>request("greeting", "Second call")
                .onItem().transform(Message::body);
    }

    private void print(Uni<String> uni) {
        uni.subscribe().with(LOG::info);
    }

}
