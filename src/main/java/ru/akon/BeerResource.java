package ru.akon;


import io.quarkus.scheduler.Scheduled;
import io.smallrye.mutiny.Multi;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.jboss.logging.Logger;
import ru.akon.dto.Beer;
import ru.akon.service.BeerService;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


@Path("/beer")
public class BeerResource {

    private static final Logger LOG = Logger.getLogger(BeerResource.class);

    @Inject
    BeerService beerService;

    @GET
    public Multi<Beer> beers() {
        LOG.info("beers");
        return Multi.createBy().repeating()
                .uni(
                        () -> new AtomicInteger(1),
                        i -> beerService.getBeers(i.getAndIncrement())
                )
                .until(List::isEmpty)
                .onItem().<Beer>disjoint()
                .select().where(b -> b.getAbv() > 0.3);
    }

    @Channel("beers")
    Emitter<Beer> emitter;

    @GET
    @Path("/emit/{beer}")
    public Response emitBeer(@PathParam("beer") int beerId) {
        LOG.info("emitBeer");

        beerService.getBeer(beerId)
                .map(beers -> beers.get(0))
                .subscribe()
                .with(emitter::send);

//        Multi.createBy().repeating()
//                .uni(
//                        () -> new AtomicInteger(1),
//                        i -> beerService.getBeers(i.getAndIncrement())
//                ).until(List::isEmpty)
//                .onItem()
//                .<Beer>disjoint()
//                .subscribe().with(emitter::send);


        return Response.ok().build();
    }

//    @Scheduled(every = "10s")
    void getAndSubscribe() {
        LOG.info("start");
        beerService.getBeers(1)
                .onItem()
                .<Beer>disjoint()
                .subscribe().with(emitter::send);
        LOG.info("finish");

    }
}