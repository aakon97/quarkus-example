package ru.akon;

import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.core.eventbus.EventBus;
import io.vertx.mutiny.core.eventbus.Message;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.jboss.logging.Logger;
import ru.akon.dto.Beer;

import java.util.concurrent.ThreadLocalRandom;

@ApplicationScoped
public class BeerProcessor {

    private static final Logger LOG = Logger.getLogger(BeerProcessor.class);


    @Incoming("beers")
    @Outgoing("messages")
    public Beer processPrize(Beer beer) {
        LOG.info("set price");
        beer.setPrice(getPrice());
        return beer;
    }

    private int getPrice() {
        LOG.info("Generate price");
        return ThreadLocalRandom.current().nextInt(1, 10);
    }

    @Inject
    EventBus bus;

    @Incoming("messages")
    public void print(Beer beer) {
        LOG.info("Print");
//        System.out.println(JsonbBuilder.create().toJson(beer));
        Uni<String> uni = sendBus();
//        uni.subscribe().with(LOG::info);
        LOG.info("123");
    }

    private Uni<String> sendBus() {
        LOG.info("start bus");
        return bus.<String>request("greeting", "PRICE")
                .onItem().transform(Message::body);
    }
}